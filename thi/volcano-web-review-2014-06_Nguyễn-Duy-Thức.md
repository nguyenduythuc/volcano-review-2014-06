# Volcano Web Review - June 2014 #

**Họ và tên: Nguyễn Duy Thức**

## Bài 01 ##

Bootstrap là một front-end framework nó chứa các công cụ miễn phí để có thể tạo các website và web application.Nó có chứa HTML và css để thiết kế cho các button,form và kho component lớn,tích hợp các jquery do họ phát triển.
 Các framework css khác:BlueTrip,BluePrint,Foundation,YAML

## Bài 02 ##

Khi muốn tạo một template nhanh và đẹp ta nên dùng bootstrap vì nó có chứa các component hữu ích cho việc phát triển.Tuy nhiên khi ta dựng teplete cho một trang lớn thì bootstrap hơi cồng kềnh và nó không tạo được sự khác biệt với các trang khác cùng sử dụng bootstrap do cùng sử dụng style của nó.

## Bài 03 ##

Giống:đều là các grid của Bootstrap có thể thay đổi kích cỡ theo màn hình.
Khác:fluid nó sẽ thay đổi kích cỡ grid theo % của màn hình,luôn chỉnh độ dài theo cửa sổ trình duyệt.Nhưng non-fluid thì kích cỡ đã được fix sẵn theo từng loại cỡ màn hình

## Bài 04 ## 

Responsive layout là layout tự điều chỉnh kích thước cho bất kỳ kiểu màn hình nào.Để một layout trở nên responsive ta phải dùng css(và một chút javascript).Trong bootstrap để sử dụng ta chỉ cần thêm class="container" vào trong thẻ div.Trong class container có chứa định nghĩa về loại màn hình và độ rộng tương ứng.

## Bài 5 ##

Bước 1:Cắt template mà ta đã dựng bằng bootstrap thành các file riêng index,header,footer
Bước 2:Đặt folder theme vừa tạo vào wp-content
Bước 3:Vào wordpress kích hoạt theme